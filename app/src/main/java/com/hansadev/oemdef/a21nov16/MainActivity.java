package com.hansadev.oemdef.a21nov16;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static com.hansadev.oemdef.a21nov16.Validation.checkDifficulty;
import static com.hansadev.oemdef.a21nov16.Validation.isEmpty;
import static com.hansadev.oemdef.a21nov16.Validation.isEnough;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tLoginStat, tPassStat, tRepPassStat, tStatus, tDebug;
    EditText etLogin, etPassword, etRepPassword;
    Button bRegister;
    RelativeLayout ma;
    int Difficulty = 0;
    int[] aDiff = new int[5];
    int[] WTF = new int[5];

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ma = (RelativeLayout) findViewById(R.id.activity_main);
        tStatus = (TextView) findViewById(R.id.t_status);
        etLogin = (EditText) findViewById(R.id.et_login);
        etPassword = (EditText) findViewById(R.id.et_password);
        etRepPassword = (EditText) findViewById(R.id.et_reppassword);
        bRegister = (Button) findViewById(R.id.b_register);
        tPassStat = (TextView) findViewById(R.id.t_passStat);
        tRepPassStat = (TextView) findViewById(R.id.t_RepPassStat);
        tLoginStat = (TextView) findViewById(R.id.t_loginStat);
        //tDebug = (TextView) findViewById(R.id.t_debug);
        bRegister.setOnClickListener(this);
        etPassword.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!(isEmpty(etPassword))) {
                    if (isEnough(etPassword)) {
                        aDiff = checkDifficulty(etPassword, WTF);
                        Difficulty = aDiff[4];
                        switch (Difficulty) {
                            case 1:
                                tPassStat.setText("Слишком простой");
                                tPassStat.setTextColor(Color.RED);
                                tStatus.setText("Пароль не должен быть слишком простым!");
                                tStatus.setTextColor(Color.RED);
                                WTF = new int[5];
                                break;
                            case 2:
                                tPassStat.setText("Простой");
                                tPassStat.setTextColor(Color.rgb(255, 153, 0));
                                WTF = new int[5];
                                break;
                            case 3:
                                tPassStat.setText("Нормальный");
                                tPassStat.setTextColor(Color.rgb(50, 205, 50));
                                WTF = new int[5];
                                break;
                            case 4:
                                tPassStat.setText("Сложный");
                                tPassStat.setTextColor(Color.rgb(0, 255, 0));
                                WTF = new int[5];
                                break;
                            default:
                                tPassStat.setText("WTF? Как так вышло?");
                                tPassStat.setTextColor(Color.rgb(255, 192, 203));
                                WTF = new int[5];
                                break;
                        }
                        if (etPassword.getText().toString().length() > 12 && Difficulty == 4) {
                            tPassStat.setText("Бог Паролей");
                            tPassStat.setTextColor(Color.rgb(0, 0, 255));
                        }
                        if (Difficulty > 1){tStatus.setTextColor(Color.GREEN);tStatus.setText("Успешный ввод пароля!");}
                        if (!isEmpty(etLogin) && Difficulty > 1 && etRepPassword.getText().toString().equals(etPassword.getText().toString())) {
                            tStatus.setTextColor(Color.GREEN);
                            tStatus.setText("Данные приняты - нажимайте кнопку :D");
                        }
                    } else {
                        tPassStat.setText("Менее 6 Символов!");
                        tPassStat.setTextColor(Color.RED);
                        tStatus.setText("Пароль должен содержать 6 символов и более!");
                        tStatus.setTextColor(Color.RED);
                    }
                } else {
                    tPassStat.setText("Поле для ввода пусто!");
                    tPassStat.setTextColor(Color.RED);
                    tStatus.setText("Одно из полей для ввода пусто!");
                    tStatus.setTextColor(Color.RED);
                }
            }
            public void beforeTextChanged(CharSequence c, int count,int start, int after) {
            }
            public void afterTextChanged(Editable c) {
            }
        });

        etLogin.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!(isEmpty(etLogin))) {
                    //Типа проверяем на доступность
                    tLoginStat.setText("Логин доступен!");
                    tLoginStat.setTextColor(Color.rgb(50, 205, 50));
                    tStatus.setTextColor(Color.GREEN);
                    tStatus.setText("Успешный ввод логина");
                    if (Difficulty > 1 && etRepPassword.getText().toString().equals(etPassword.getText().toString())) {
                        tStatus.setTextColor(Color.GREEN);
                        tStatus.setText("Данные приняты - нажимайте кнопку :D");
                    }
                } else {
                    tLoginStat.setText("Поле для ввода пусто!");
                    tLoginStat.setTextColor(Color.RED);
                    tStatus.setText("Одно из полей для ввода пусто!");
                    tStatus.setTextColor(Color.RED);
                }
            }
            public void beforeTextChanged(CharSequence c, int count,int start, int after) {
            }
            public void afterTextChanged(Editable c) {
            }
        });

        etRepPassword.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (isEmpty(etRepPassword)) {
                    tRepPassStat.setText("Поле для ввода пусто!");
                    tRepPassStat.setTextColor(Color.RED);
                    tStatus.setText("Одно из полей для ввода пусто!");
                    tStatus.setTextColor(Color.RED);
                } else if (etRepPassword.getText().toString().equals(etPassword.getText().toString())) {
                    tRepPassStat.setText("Пароли совпали!");
                    tRepPassStat.setTextColor(Color.rgb(50, 205, 50));
                    tStatus.setTextColor(Color.GREEN);
                    tStatus.setText("Успешное подтверждение пароля");
                    if (!isEmpty(etLogin) && Difficulty > 1) {
                        tStatus.setTextColor(Color.GREEN);
                        tStatus.setText("Данные приняты - нажимайте кнопку :D");
                    }
                } else {
                    tRepPassStat.setText("Пароли не совпадают!");
                    tRepPassStat.setTextColor(Color.RED);
                }
            }
            public void beforeTextChanged(CharSequence c, int count,int start, int after) {
            }
            public void afterTextChanged(Editable c) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        Log.d("MainActivity", "OnClick");

        if (isEmpty(etPassword) | isEmpty(etLogin) | isEmpty(etRepPassword)) {
        tStatus.setTextColor(Color.RED);
        tStatus.setText("Одно из полей для ввода пусто!");
        } else if (!(isEnough(etPassword))) {
            tStatus.setTextColor(Color.RED);
            tStatus.setText("Пароль должен содержать 6 символов и более!");
        } else if (Difficulty < 2) {
            tStatus.setTextColor(Color.RED);
            tStatus.setText("Пароль не должен быть слишком простым!");
        } else if (!(etRepPassword.getText().toString().equals(etPassword.getText().toString()))) {
            tStatus.setTextColor(Color.RED);
            tStatus.setText("Пароли не совпадают!");
        } else {
            tStatus.setTextColor(Color.GREEN);
            tStatus.setText("Выполняется вход...");
            Intent intent = new Intent(this, GreetingScreenActivity.class);
            intent.putExtra("login", etLogin.getText().toString());
            startActivity(intent);
        }
    }
}
