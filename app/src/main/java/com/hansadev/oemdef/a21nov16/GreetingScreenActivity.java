package com.hansadev.oemdef.a21nov16;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class GreetingScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.greeting_screen);


        RelativeLayout ga = (RelativeLayout) findViewById(R.id.greeting_screen);
        TextView tGreeting = (TextView) findViewById(R.id.t_greeting);

        Intent intent = getIntent();
        String username = intent.getExtras().getString("login");

        ga.setBackgroundColor(Color.BLUE);
        tGreeting.setTextColor(Color.WHITE);

        tGreeting.setText("Добро пожаловать, " + username);


    }
}
