package com.hansadev.oemdef.a21nov16;

import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Character.isDigit;
import static java.lang.Character.isLetter;
import static java.lang.Character.isLowerCase;
import static java.lang.Character.isWhitespace;

public class Validation {

    public static boolean isEnough(EditText ET) {
        if (ET.getText().toString().length() < 6) {
            return false;
        } else {
            return true;
        }
    }

    public static int[] checkDifficulty(EditText ET, int[] WTF) {


        String STR = ET.getText().toString();
        char[] String = STR.toCharArray();

        for (int i = 0; i < String.length; i++) {
            if (isDigit(String[i])) {WTF[0] = 1;}
            else if (isLetter(String[i])) {
                if (isLowerCase(String[i])) {WTF[1] = 1;} else {WTF[2] = 1;}
            } else if (!isWhitespace(String[i])) {WTF[3] = 1;}
        }

        for (int i = 0; i < 4; i++) {
            if (WTF[i] != 0) {
                WTF[4]++;
            }
        }

        return WTF;
    }

    public static boolean isEmpty(EditText ET) {
        if (ET.getText().toString().equals("")) {
            return true;
        } else {
            return false;
        }
    }
}
